<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Turing!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Végtelen ciklus</title>
        <para>
            Írj olyan C végtelen ciklusokat, amelyek 0 illetve 100 százalékban dolgoztatnak egy magot és egy olyat, amely  
            100 százalékban minden magot!
        </para>
        <para>
            Megoldás videó:
            <itemizedlist>
                <listitem>
                    <para><link xlink:href="https://youtu.be/HEKlkMrlXvU">inf_single_f</link></para>                        
                </listitem>
                <listitem>
                    <para><link xlink:href="https://youtu.be/62aoJP1swZQ">inf_single_w</link></para>
                </listitem>
                <listitem>
                    <para><link xlink:href="https://youtu.be/Wo6Yc5AzkRg">notFull_single</link></para>
                </listitem>
                <listitem>
                    <para><link xlink:href="https://youtu.be/AcYHuK-cS1w">full_multiple</link></para>
                </listitem>
            </itemizedlist>
        </para>
        <para>
            Megoldás forrása:
            <itemizedlist>
                <listitem>
                    <para>
                        <link xlink:href="Turing/infCycles/inf_single_f.cpp"><filename>/Turing/infCycles/inf_single_f.cpp</filename></link>
                    </para>
                </listitem>
                <listitem>
                    <para>
                        <link xlink:href="Turing/infCycles/inf_single_w.cpp"><filename>/Turing/infCycles/inf_single_w.cpp</filename></link>
                    </para>
                </listitem>
                <listitem>
                    <para>
                        <link xlink:href="Turing/infCycles/notFull_single.cpp"><filename>/Turing/infCycles/notFull_single.cpp</filename></link>
                    </para>
                </listitem>
                <listitem>
                    <para>
                        <link xlink:href="Turing/infCycles/full_multiple.cpp"><filename>/Turing/infCycles/full_multiple.cpp</filename></link>
                    </para>
                </listitem>
            </itemizedlist>
        </para>
        <para>
            Sok féle módszerrel hozhatunk létre végtelen ciklusokat (lásd <literal>for</literal> és <literal>while</literal> megoldás).
            Vannak olyan esetek, mikor kifejezetten ez a célunk, például egy szolgáltatás futásának garantálása, de gyakran olyanra is van példa, amikor kifejezetten nem ez a célunk (bug).
            Ebben a feladatsorba erre egy példa a PageRank algoritmus is, ahol csak egy adott ideig kéne konvergálnia az algoritmusnak, holott egy apró hiba esetén végtelen ciklus jön létre. Ezek megoldására többnyire egy feltétel bevezetése (while fejléce, vagy break parancs) megoldja a problémát.
        </para>
               <para>
            Processzor 1 magjának 100%-os terheltsége:               
        </para>
        <programlisting language="c++"><![CDATA[    
int main(){
    for(;;){}
    return 0;
}]]>
        </programlisting>        
        <para>        
        vagy az olvashatóbb, de a programozók és fordítók (szabványok) között kevésbé hordozható (Miért is az?)
        </para>
        <programlisting language="c++"><![CDATA[
int main(){
    while(true);
    return 0;
}]]>
        </programlisting>        
        <para>
            Azért érdemes a <literal>for(;;)</literal> hagyományos formát használni, 
            mert ez minden C szabvánnyal lefordul, másrészt
            a többi programozó azonnal látja, hogy az a végtelen ciklus szándékunk szerint végtelen és nem szoftverhiba. 
            Mert ugye, ha a <literal>while</literal>-al trükközünk egy nem triviális 
            <literal>1</literal> vagy <literal>true</literal> feltétellel, akkor ott egy másik, a forrást
            olvasó programozó nem látja azonnal a szándékunkat, de az is fennállhat, hogy a fordítónak nem tetszik a <literal>true</literal>, mint feltétel a fejlécben (pl.: C#).
        </para>            
        <para>
            A fordító a <literal>for</literal>-os és 
            <literal>while</literal>-os ciklusból ugyanazt az assembly kódot fordítja:
        </para>            
        <screen><![CDATA[
$ gcc -S -o inf_single_f.S inf_single_f.cpp   
$ gcc -S -o inf_single_w.S inf_single_w.cpp  
$ diff inf_single_w.S inf_single_f.S 
1c1
<   .file   "inf_single_w.cpp"
---
> ]]>
        </screen>  
        <para>
            Egy processzor mag 0%-ban:               
        </para>        
        <programlisting language="c++"><![CDATA[
#include <thread>
#include <chrono>
int main(){
    for(;;)
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    return 0;
}]]>
        </programlisting>        
        <para>
            Minden processzor mag 100%-ban:               
        </para>

        <programlisting language="c++"><![CDATA[
#include <omp.h>
int main (){
    #pragma omp parallel
    {
      for (;;);
    }
    return 0;
}]]>
        </programlisting>        
        <para>
            A <command>gcc full_multiple.cpp -o full_multiple -fopenmp</command> parancssorral készítve a futtatható állományt, majd futtatva,               
            közben egy másik terminálban a <command>top</command> parancsot kiadva tanulmányozzuk, mennyi CPU-t használunk:            
        </para>
        <screen><![CDATA[
top - 23:23:18 up  2:05,  1 user,  load average: 4,00, 3,17, 2,56
Tasks: 296 total,   3 running, 293 sleeping,   0 stopped,   0 zombie
%Cpu0: 100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu1: 100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu2: 100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu3: 100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu4: 100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu5: 100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu6: 100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu7: 100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
MiB Mem : 7292,8 total, 2993,9 free, 1160,7 used, 3138,2 buff/cache
MiB Swap: 2048,0 total, 2027,7 free,   20,2 used. 5585,7 avail Mem

  PID USER  PR NI  VIRT RES SHR S  %CPU %MEM   TIME+ COMMAND
18647 hpeti 20  0 60256 808 712 R 800,0  0,0 2:12.76 full_mult+
]]></screen> 
    </section>        
        
    <section>
        <title>Lefagyott, nem fagyott, akkor most mi van?</title>
        <para>
            Mutasd meg, hogy nem lehet olyan programot írni, amely bármely más programról eldönti, hogy le fog-e fagyni vagy sem!
        </para>
        <para>
            Megoldás videó:
        </para>
        <para>
            Megoldás forrása:  tegyük fel, hogy akkora haxorok vagyunk, hogy meg tudjuk írni a <function>Lefagy</function>
            függvényt, amely tetszőleges programról el tudja dönteni, hogy van-e benne végtelen ciklus:              
        </para>
        <programlisting language="c"><![CDATA[
Program T100{
	boolean Lefagy(Program P){
		 if(P-ben van végtelen ciklus)
			return true;
		 else
			return false; 
	}

	main(Input Q){
		Lefagy(Q)
	}
}]]>
	</programlisting>            
        <para>
            A program futtatása, például akár az előző <filename>v.c</filename> ilyen pszeudókódjára:
            <screen><![CDATA[T100(t.c.pseudo)
true]]></screen>            
            akár önmagára
            <screen><![CDATA[T100(T100)
false]]></screen>  
            ezt a kimenetet adja.          
        </para>
        <para>
            A T100-as programot felhasználva készítsük most el az alábbi T1000-set, amelyben a
            Lefagy-ra épőlő Lefagy2 már nem tartalmaz feltételezett, csak csak konkrét kódot:
        </para>
        <programlisting language="c"><![CDATA[
Program T1000{
	boolean Lefagy(Program P){
		 if(P-ben van végtelen ciklus)
			return true;
		 else
			return false; 
	}
	boolean Lefagy2(Program P){
		 if(Lefagy(P))
			return true;
		 else
			for(;;); 
	}

	main(Input Q){
		Lefagy2(Q)
	}
}]]>
		</programlisting>        
        <para>
            Mit for kiírni erre a <computeroutput>T1000(T1000)</computeroutput> futtatásra?      
            <itemizedlist>
                <listitem>
                    <para>Ha T1000 lefagyó, akkor nem fog lefagyni, kiírja, hogy true</para>                        
                </listitem>
                <listitem>
                    <para>Ha T1000 nem fagyó, akkor pedig le fog fagyni...</para>                        
                </listitem>
            </itemizedlist>
            akkor most hogy fog működni? Sehogy, mert ilyen <function>Lefagy</function>
            függvényt, azaz a T1000 program nem is létezik.
        </para>
        <para>
            Lényegében nem tudjuk meghatározni, hogy most vé
        </para>
    </section>        
                
    <section>
        <title>Változók értékének felcserélése</title>
        <para>
            Írj olyan C programot, amely felcseréli két változó értékét, bármiféle logikai utasítás vagy kifejezés
            nasználata nélkül!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/HzVTB843FIc">https://youtu.be/HzVTB843FIc</link>
        </para>
        <para>
            Megoldás forrása:  <link xlink:href="Turing/swapping/swapping.cpp">
                <filename>/Turing/swapping/swapping.cpp</filename>
            </link>
        </para>
        <programlisting language="c++"><![CDATA[
#include <iostream>
using namespace std;

void swapArithmetic(int &a, int &b);
void swapXOR(int &a, int &b);

int main(){
    int a = 23;
    int b = 7;
    cout << "Original a: "<< a << " original b: " << b << endl << endl;

    swapArithmetic(a,b);
    cout << "Arithmetric method:" <<endl;
    cout << "Swapped a: "<< a << " swapped b: " << b << endl << endl;

    a = 23; //restoring original values
    b = 7;
    swapXOR(a,b);
    cout << "XOR method:" << endl;
    cout << "Swapped a: " << a << " swapped b: " << b << endl;
    return 0;
}
void swapArithmetic(int &a, int &b){
    a = a + b; //a += b;
    b = a - b;
    a = a - b; //a -= b;
}
void swapXOR(int &a, int &b){
    //in this case a = 23 = 10111 (binary)
    //in this case b = 7  = 00111 (binary)
    a = a ^ b; //10000
    b = a ^ b; //10111 = original a's value
    a = a ^ b; //00111 = original b's value
}]]>
        </programlisting>
        <para>
            A változók megcserélésére segédváltozó nélkül többféle módszer is van, ebben a részben az aritmetikai (+ és -) és a kizáró-vagy (XOR) segítségével cserélem meg két változó értékét (számokat).

            (képek beillesztése később...)
        </para>
    </section>                     

    <section>
        <title>Labdapattogás</title>
        <para>
            Először if-ekkel, majd bármiféle logikai utasítás vagy kifejezés
            nasználata nélkül írj egy olyan programot, ami egy labdát pattogtat a karakteres konzolon! (Hogy mit értek
            pattogtatás alatt, alább láthatod a videókon.)
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/gzS3Xpr31_c">https://youtu.be/gzS3Xpr31_c</link>
        </para>
        <para>
            Megoldás forrása:  <link xlink:href="Turing/bouncingBall/bouncingBall_if.cpp">
                <filename>Turing/bouncingBall/bouncingBall_if.cpp</filename></link>
        </para>
        <para>
            Első megoldás: if-ekkel
        </para>
        <programlisting language="c++"><![CDATA[
#include <curses.h>
#include <unistd.h>
using namespace std;
int main(){
    WINDOW *console = initscr();

    int x = 0;
    int y = 0;
    int xIncr = 1;
    int yIncr = 1;
    int max_X;
    int max_Y;

    for(;;){
        getmaxyx(console, max_Y, max_X); //changing max sizes
        mvprintw(y, x, "O");
        refresh();
        usleep(50000);
        clear();
        x += xIncr;
        y += yIncr;

        if(x >= max_X-1)
            xIncr *= -1;
        if(x <= 0)
            xIncr *= -1;
        if(y >= max_Y-1)
            yIncr *= -1;
        if(y <= 0)
            yIncr *= -1;
    }
    return 0;
}]]>
        </programlisting>
        <para>
            A program írásához és fordításához szükséges pár package-t feltelepíteni rendszerünkre:
            <screen><![CDATA[sudo apt-get install libncurses6 libncurses6-dev libncursesw6]]></screen>
            Program fordítása:
            <screen><![CDATA[g++ bouncingBall_if.cpp -o bouncingBall_if -lncurses]]></screen>

            Tanulságok, tapasztalatok, magyarázat...
        </para>
    </section> 

    <section>
        <title>Szóhossz és a Linus Torvalds féle BogoMIPS</title>
        <para>
            Írj egy programot, ami megnézi, hogy hány bites a szó a gépeden, azaz mekkora az <type>int</type> mérete.
            Használd ugyanazt a while ciklus fejet, amit Linus Torvalds a BogoMIPS rutinjában! 
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/wgmP9lWvM48">https://youtu.be/wgmP9lWvM48</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Turing/wordLength/wordLength.cpp">
                <filename>Turing/wordLength/wordLength.cpp</filename>
            </link>
        </para>
        <programlisting language="c++"><![CDATA[
#include <stdio.h>
#include <iostream>
using namespace std;
int main(){
    unsigned long long int wordLength = 0, word = 1;
    do{
        wordLength++;
        printf("ez a szám: %llu \n",word);
    }
    while(word <<= 1);
    //wordLength++;
    printf("The word length on this PC is %llu bits\n", wordLength);
    return 0;
}]]>
		</programlisting>
        <para>
            A számítógép processzor regiszterének nagysága adja meg, hogy mekkora memóriacímekkel dolgozzon a rendszer/kernel. Ez azt jelenti, hogy a programunkba az unsigned long long int változótípusba (előjel nélküli) megnézzük hány bit eltolás műveletet tudunk végrehajtani. Ez megadja nekünk, hogy mekkora szóhosszal dolgozik a rendszerünk jelen esetben.
            Eredetileg, a bogomips fájlban sima előltesztelő ciklus van megadva, de ezt átírva hátultesztelő ciklusra megelőzzük az utólagos +1 értéknövelést.
            [insert image here]
            Segítséget adott: Halász Dániel 
        </para>
    </section>                     

    <section>
        <title>Helló, Google!</title>
        <para>
            Írj olyan C programot, amely egy 4 honlapból álló hálózatra kiszámolja a négy lap Page-Rank 
            értékét!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/VRbFT2PlroQ">https://youtu.be/VRbFT2PlroQ</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Turing/PageRank/pagerank.cpp"></link>
        </para>
        <programlisting language="c++"><![CDATA[
#include <stdio.h>
#include <math.h>
#include <iostream>
using namespace std;
//init forrás: https://progpater.blog.hu/2011/02/13/bearazzuk_a_masodik_labort
void printRanks (double ranks[], int n){
	for(int i = 0; i < n; i++){
        cout << ranks[i] << endl;
    }
}
double distance (double PR[], double PRv[], int n){
    double sum = 0;
	for(int i = 0; i < n; i++){
		sum+= (PRv[i]-PR[i]) * (PRv[i] - PR[i]);
	}
	return sqrt(sum); //return distance
}
void calcPageRank(double linkData[4][4]){
    double PR[4] = {0.0, 0.0, 0.0, 0.0}; //it's easier to put these here
	double initPR[4] = {1.0/4.0, 1.0/4.0, 1.0/4.0, 1.0/4.0};//rather than using parameters
    for(;;){
        for(int i = 0; i < 4; i++){
            PR[i] = 0;
            for(int j = 0; j < 4; j++){
                PR[i] += linkData[i][j] * initPR[j]; //this is a matrix multiplication
            }
        }
        if(distance(PR, initPR, 4) < 0.0000000001){ //damping factor
            break;
            }
        for(int i = 0; i < 4;i++){
            initPR[i] = PR[i];
        }
    }
    printRanks(PR, 4);
}
int main (){
	double linkData_normal[4][4] =
	{
        {0.0,     0.0, 1.0/3.0, 0.0},
        {1.0, 1.0/2.0, 1.0/3.0, 1.0},
        {0.0, 1.0/2.0,     0.0, 0.0},
        {0.0,     0.0, 1.0/3.0, 0.0}
	};
    calcPageRank(linkData_normal);
	return 0;
}]]>
        </programlisting>
        <para>
            Lényegében négyzetes mátrixot használunk az alap információk eltárolására, azaz oszloponként más-más 'objektumot' jellemzünk olyan módon, hogy milyen más objektumra mutat rá. [insert image here]
        </para>
        <para>A program írása során érdekes volt megoldani a belső metódusoknak lehetővé tenni azt, hogy</para>
    </section>

    <section xml:id="Brun">
        <title>100 éves a Brun tétel</title>
        <para>
            Írj R szimulációt a Brun tétel demonstrálására!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/hmnP6rc896g">https://youtu.be/hmnP6rc896g</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Turing/brun/brun.r">Turing/brun/brun.r</link>
        </para>
        <para>
            Én a programot linuxon írtam meg és futtattam.
            Első lépésünk, hogy telepítsük az R nyelvet (ubuntu szoftverek -> R).
            Ezután elindítunk egy terminalt és az alábbi parancsokat beírjuk:
        </para>
        <screen><![CDATA[
hpeti@hpeti-Lenovo-V330-15IKB:~$ R
...
> install package("matlab")]]>
        </screen>
        
        <para>Az előző parancsba telepítettük a matlab csomagot az R-en belül, innestől kezdve ezt el tudjuk érni a programból, de be kell importálni a programba mindig. (lásd <literal>library()</literal>)</para>
        <programlisting><![CDATA[
library(matlab)

stp <- function(x){

    primes = primes(x)
    difference = primes[2:length(primes)]-primes[1:length(primes)-1]
    twin_index = which(difference==2)
    t1primes = primes[twin_index]
    t2primes = primes[twin_index]+2
    reciprocal_sum = 1/t1primes + 1/t2primes
    return(sum(reciprocal_sum))
}

x=seq(13, 1000000, by=10000)
y=sapply(x, FUN = stp)
plot(x,y,type="b")]]>            
        </programlisting>
        <para>Első megoldás a program futtatására:</para>
        <para>A programomat a Visual Studio Code-ban futtattam le, ahol hozzáadtam az R package-t.</para>
        <para>
        	<literal>Ctrl+Shift+S</literal> Ez megnyitja a terminált R környezettel (R parancs).
        	Először is kijelöljük a futtatandó sorokat (<literal>Ctrl+A</literal> ebben az esetben) és ezután <literal>Ctrl+Enter</literal>-t megnyomjuk és soronként beírja a lenti terminálba a sorokat egyesével, amit az R környezet parancsként dolgoz fel.
    	</para>
        <para>Tény és való, ez nem a legszebb módszer a programunk lefuttatására ezért...</para>
        <para>Második megoldás a program futtatására:</para>
        <para>Nyitunk egy terminált és abba a mappába navigálunk ahol megtalálható a programunk (brun.r). Ezután a következő parancssal elindítjuk a programunkat:</para>
        <screen><![CDATA[
Rscript brun.r]]>
        </screen>
        <para>Az ábrázolt reciprokösszegeken látszik, hogy egy adott határt nem lépnek át az ábrázolt értékek, de ezek az értékek a végtelenhez tartanak. Ezt a határt hívjuk Brun-konstanstnak.</para>
        
        <mediaobject>
                <imageobject><imagedata fileref="/Turing/brun/brun.png" scale="50" /></imageobject>
                <textobject><phrase>A B<subscript>2</subscript>(Brun-konstans) megközelítése</phrase></textobject>
        </mediaobject>
    </section>
    
    <section xml:id="bhax-textbook-feladatok-turing.MontyHall">
        <title>A Monty Hall probléma</title>
        <para>
            Írj R szimulációt a Monty Hall problémára!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://bhaxor.blog.hu/2019/01/03/erdos_pal_mit_keresett_a_nagykonyvben_a_monty_hall-paradoxon_kapcsan">https://bhaxor.blog.hu/2019/01/03/erdos_pal_mit_keresett_a_nagykonyvben_a_monty_hall-paradoxon_kapcsan</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/MontyHall_R">https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/MontyHall_R</link>
        </para>
        <programlisting language="r"><![CDATA[
number_of_tries=10000000
tries = sample(1:3, number_of_tries, replace=T)
jatekos = sample(1:3, number_of_tries, replace=T)
musorvezeto=vector(length = kiserletek_szama)
    for (i in 1:number_of_tries) {
        if(tries[i]==jatekos[i]){
            mibol=setdiff(c(1,2,3), kiserlet[i])
        }
        else{
             mibol=setdiff(c(1,2,3), c(kiserlet[i], jatekos[i])) 
        }
        musorvezeto[i] = mibol[sample(1:length(mibol),1)]
    }
nemvaltoztatesnyer= which(kiserlet==jatekos)
valtoztat=vector(length = kiserletek_szama)
    for (i in 1:number_of_tries) {
        holvalt = setdiff(c(1,2,3), c(musorvezeto[i], jatekos[i]))
        valtoztat[i] = holvalt[sample(1:length(holvalt),1)]            
    }
valtoztatesnyer = which(kiserlet==valtoztat)
sprintf("Kiserletek szama: %i", kiserletek_szama)
length(nemvaltoztatesnyer)
length(valtoztatesnyer)
length(nemvaltoztatesnyer)/length(valtoztatesnyer)
length(nemvaltoztatesnyer)+length(valtoztatesnyer)
]]>          
        </programlisting>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
    </section>

</chapter>                
