//Program written by HPeti
int* returnIntPointer(int* intPointer){
    //this is for 7th
    return intPointer;
}
int sumOfInts(int x, int y){
    return (x+y);
}
int somethingFor9th(int x){
    int (*methodPointer)(int, int);
    methodPointer = &sumOfInts;
    return methodPointer(x,x);
    //return int (*sumOfInts(x,x))(int); //this ain't right...
}
int main(){
    //Ezek a megoldandó feladatok:
    //1.    egész 
    //2.    egészre mutató mutató
    //3.    egész referenciája
    //4.    egészek tömbje
    //5.    egészek tömbjének referenciája(nem az első elem!)
    //6.    egészre mutató mutatók tömbje
    //7.    egészre mutató mutatót visszaadó függvény
    //8.    egészre mutató mutatót visszaadó függvényre mutató mutató
    //9.    egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény
    //      TRANSLATE: egy egészet kapó és egészet visszaadó függvény, ami egy két egészet kapó függvényre mutató mutatót visszaadó függvényt használ
    //10.   függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függényre
    
    int a = 86; //1.
    int* b = &a; //2.
    int &ra = a; //3.
    int intArray[4] = {1,2,3,4}; //4.
    int  (&refArray)[4] = intArray; //5.
    int* pointerArray[3] = {b,b,b}; //6.
    returnIntPointer(b); //7.
    int* pointerFromMethod = returnIntPointer(b); //8
    somethingFor9th(a); //9.
    void (*somethingFor9th)(int); //10.
}
