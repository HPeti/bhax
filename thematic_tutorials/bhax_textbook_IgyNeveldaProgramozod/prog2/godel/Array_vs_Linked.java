import java.util.ArrayList;
import java.util.LinkedList;

public class Array_vs_Linked {
    public static double getSeconds(long nano) {
        return (double) nano / 1000000000;
    }

    public static void main(String[] args) {
        int size = 0;
        if (args.length == 0)
            size = 100000;
        else
            size = Integer.parseInt(args[0]);

        ArrayList arraylist = new ArrayList();
        LinkedList linkedlist = new LinkedList();

        // arraylist add
        long startTime = System.nanoTime();
        for (int i = 0; i < size; i++) {
            arraylist.add(i);
        }
        long endTime = System.nanoTime();
        long duration = endTime - startTime;
        System.out.println("arraylist add:  " + getSeconds(duration) + " sec");

        // linkedlist add
        startTime = System.nanoTime();
        for (int i = 0; i < size; i++) {
            linkedlist.add(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("linkedlist add: " + getSeconds(duration) + " sec");

        // arraylist get
        startTime = System.nanoTime();
        for (int i = 0; i < size; i++) {
            arraylist.get(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("arraylist get:  " + getSeconds(duration) + " sec");

        // linkedlist get
        startTime = System.nanoTime();
        for (int i = 0; i < size; i++) {
            linkedlist.get(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("linkedlist get: " + getSeconds(duration) + " sec");

        // arraylist remove
        startTime = System.nanoTime();
        for (int i = 0; i < size/2; i++) {
            arraylist.remove(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("arraylist remove:  " + getSeconds(duration) + " sec");

        // linkedlist remove
        startTime = System.nanoTime();
        for (int i = 0; i < size/2; i++) {
            linkedlist.remove(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("linkedlist remove: " + getSeconds(duration) + " sec");
    }
}
