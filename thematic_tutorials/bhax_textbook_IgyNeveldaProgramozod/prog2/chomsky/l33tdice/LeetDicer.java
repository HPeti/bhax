import java.io.*;
import java.util.HashMap;
import java.util.Random;

public class LeetDicer {
    private HashMap<String, String[]> leets = new HashMap<>();

    public LeetDicer() {
        leets.put("a", new String[]{"4", "4", "@", "/-\\"});
        leets.put("b", new String[]{"b", "8", "|3", "|}"});
        leets.put("c", new String[]{"c", "(", "<", "{"});
        leets.put("d", new String[]{"d", "|)", "|]", "|}"});
        leets.put("e", new String[]{"3", "3", "3", "3"});
        leets.put("f", new String[]{"f", "|=", "ph", "|#"});
        leets.put("g", new String[]{"g", "6", "[", "[+"});
        leets.put("h", new String[]{"h", "4", "|-|", "[-]"});
        leets.put("i", new String[]{"1", "1", "|", "!"});
        leets.put("j", new String[]{"j", "7", "_|", "_/"});
        leets.put("k", new String[]{"k", "|<", "1<", "|{"});
        leets.put("l", new String[]{"l", "1", "|", "|_"});
        leets.put("m", new String[]{"m", "44", "(V)", "|\\/|"});
        leets.put("n", new String[]{"n", "|\\|", "/\\/", "/V"});
        leets.put("o", new String[]{"0", "0", "()", "[]"});
        leets.put("p", new String[]{"p", "/o", "|D", "|o"});
        leets.put("q", new String[]{"q", "9", "O_", "(,)"});
        leets.put("r", new String[]{"r", "12", "12", "|2"});
        leets.put("s", new String[]{"s", "5", "$", "$"});
        leets.put("t", new String[]{"t", "7", "7", "|"});
        leets.put("u", new String[]{"u", "|_|", "(_)", "[_]"});
        leets.put("v", new String[]{"v", "\\/", "\\/", "\\/"});
        leets.put("w", new String[]{"w", "VV", "\\/\\/", "(/\\)"});
        leets.put("x", new String[]{"x", "%", ")(", ")("});
        leets.put("y", new String[]{"y", "y", "y", "y"});
        leets.put("z", new String[]{"z", "2", "7_", ">_"});

        leets.put("0", new String[]{"D", "0", "D", "0"});
        leets.put("1", new String[]{"I", "I", "L", "L"});
        leets.put("2", new String[]{"Z", "Z", "Z", "e"});
        leets.put("3", new String[]{"E", "E", "E", "E"});
        leets.put("4", new String[]{"h", "h", "A", "A"});
        leets.put("5", new String[]{"S", "S", "S", "S"});
        leets.put("6", new String[]{"b", "b", "G", "G"});
        leets.put("7", new String[]{"T", "T", "j", "j"});
        leets.put("8", new String[]{"X", "X", "X", "X"});
        leets.put("9", new String[]{"g", "g", "j", "j"});
    }

    private String translateChar(char key) {
        if (leets.get(Character.toString(key)) != null) {
            return leets.get(Character.toString(key))[new Random().nextInt(4)];
        }
        return "";
    }

    public String toLeet(String text) {
        StringBuilder builder = new StringBuilder();
        for (char item : text.toCharArray()) {
            String temp = translateChar(item);
            if (!temp.equals("")) {
                builder.append(temp);
            } else {
                builder.append(item);
            }
        }
        return builder.toString();
    }

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out));
        if (args.length == 1) {
            FileWriter file = new FileWriter(args[0]);
            writer = new BufferedWriter(file);
        }
        LeetDicer leeter = new LeetDicer();
        String input = reader.readLine();
        while (input.length() != 0) {
            writer.write(leeter.toLeet(input));
            writer.newLine();
            input = reader.readLine();
        }
        writer.close();
        reader.close();
    }
}
