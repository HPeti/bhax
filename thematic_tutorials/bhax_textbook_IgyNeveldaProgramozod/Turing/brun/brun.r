library(matlab) #importing matlab functions

tp_values <- function(x){

    primes = primes(x)
    difference = primes[2:length(primes)]-primes[1:length(primes)-1]
    twin_index = which(difference==2)
    t1primes = primes[twin_index]
    t2primes = primes[twin_index]+2
    reciprocal_sum = 1/t1primes + 1/t2primes
    return(sum(reciprocal_sum))
}

x=seq(13, 1000000, by=10000) #seq(from 13, to 1000000, by=10000 increment)
y=sapply(x, FUN = tp_values) #sapply(vector, FUN = method)
plot(x,y,type="b")

