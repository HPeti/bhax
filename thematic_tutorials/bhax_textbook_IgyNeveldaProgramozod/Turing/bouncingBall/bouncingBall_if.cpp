#include <curses.h>
#include <unistd.h>
using namespace std;
int main(){
    WINDOW *console = initscr();

    int x = 0;
    int y = 0;
    int xIncr = 1;
    int yIncr = 1;
    int max_X;
    int max_Y;

    for(;;){
        getmaxyx(console, max_Y, max_X); //changing max sizes
        mvprintw(y, x, "O");
        refresh();
        usleep(50000);
        clear();
        x += xIncr;
        y += yIncr;

        if(x >= max_X-1)
            xIncr *= -1;
        if(x <= 0)
            xIncr *= -1;
        if(y >= max_Y-1)
            yIncr *= -1;
        if(y <= 0)
            yIncr *= -1;
    }
    return 0;
}
