(defun factorial (szam)
    (if (= szam 0)
        1
        (* szam (factorial (- szam 1)))
    )
)
; (fakt 5)
(format t "A faktoriális eredménye(5): ~D" (factorial 5))
