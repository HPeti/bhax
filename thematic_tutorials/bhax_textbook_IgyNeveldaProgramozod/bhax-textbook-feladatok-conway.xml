<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Conway!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Hangyaszimulációk</title>
        <para>
            Írj Qt C++-ban egy hangyaszimulációs programot, a forrásaidról utólag reverse engineering jelleggel
            készíts UML osztálydiagramot is!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://bhaxor.blog.hu/2018/10/10/myrmecologist">https://bhaxor.blog.hu/2018/10/10/myrmecologist</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/HPeti/bhax/-/tree/master/attention_raising/Myrmecologist">GitLab/HPeti/bhax/attention_raising/Myrmecologist</link>    
        </para>
        <para>
            Futtatás videója: <link xlink:href="https://youtu.be/yQ816p-H_kQ">https://youtu.be/yQ816p-H_kQ</link>
        </para>
        <para>
            A program fordítása a következő:
            <screen><![CDATA[
qmake myrmecologist.pro
make]]>
            </screen>
        </para>
        <para>
            A futtatási kapcsolók a következők (Ezek befordításáért a <literal>main.cpp</literal> felel):
        </para>
        <itemizedlist>
            <listitem>
                <para>w - Oszlopok számának megadása (cellák száma)</para>
            </listitem>
            <listitem>
                <para>m - Sorok számának megadása (cellák száma)</para>
            </listitem>
            <listitem>
                <para>n - Hangyák száma</para>
            </listitem>
            <listitem>
                <para>t - Két lépés között eltelt idő (millisec)</para>
            </listitem>
            <listitem>
                <para>p - Feromon párolgásának mértéke</para>
            </listitem>
            <listitem>
                <para>f - A hagyott feromon nyom értéke (konkrétan mennyire legyen zöld)</para>
            </listitem>
            <listitem>
                <para>s - A hagyott nyom értéke a szomszédokban</para>
            </listitem>
            <listitem>
                <para>d - Induló érték a cellákban</para>
            </listitem>
            <listitem>
                <para>a - Cella maximum értéke (feromon értéke)</para>
            </listitem>
            <listitem>
                <para>i - Cella minimum értéke (feromon értéke)</para>
            </listitem>
            <listitem>
                <para>c - Hány darab hangya férjen el egy cellában maximum</para>
            </listitem>
        </itemizedlist>
        <para>
            A példaszimuláció elindítása a fordítás után:
            <screen><![CDATA[
./myrmecologist -w 250 -m 150 -n 400 -t 10 -p 5 -f 80 -d 0 -a 255 -i 3 -s 3  -c 22]]>
            </screen>
        </para>
        <figure>
            <title>A példaszimuláció futtatása</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Conway/hangya.png" scale="30"/>
                </imageobject>
                <textobject>
                    <phrase>A példa hangyaszimuláció futtatása</phrase>
                </textobject>
            </mediaobject>
        </figure>
        <para>
            A programról még egy UML ábrát is kell létrehoznunk olyan szempontokkal, hogy látszódjon az osztály neve, az adattagjai és a metódusai.
            Ezek rendelkeznek <literal>private</literal>(UML esetében - előtag) <literal>public</literal> (UML esetében + előtag) láthatósággal. Az osztályok nyilakkal is össze vannak kötve, ahol egymásnak információt adhatnak át.
        </para>
        <para>
            Az UML-es ábrákkal leírhatjuk a teljes programszerkezetet, akár ismerjük normálisan a metódusok neveit, vagy hogy egyáltalán pontosan mit is csinálnak. Így emiatt kapcsolódik a <literal>reverse engineering</literal> témához is.
        </para>
        <figure>
            <title>A program UML ábrája</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Conway/uml_hangya.png" scale="30"/>
                </imageobject>
                <textobject>
                    <phrase>A program UML ábrája</phrase>
                </textobject>
            </mediaobject>
        </figure>
        <para>

        </para>
    </section>        
    <section>
        <title>Java életjáték</title>
        <para>
            Írd meg Java-ban a John Horton Conway-féle életjátékot, 
            valósítsa meg a sikló-kilövőt!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/CCQcpxKGzg8">https://youtu.be/CCQcpxKGzg8</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/HPeti/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/eletjatek_java/Sejtautomata.java">GitLab/HPeti/bhax/Conway/eletjatek_java/Sejtautmata.java</link>               
        </para>
        <para>
            Ezt a játékot másnéven Sejtautomatának nevezzük. A négyzetrács mezőit celláknak, a korongokat sejteknek nevezzük. Egy cella környezete a hozzá legközelebb eső 8 mező (átlós cellákat is figyelembe vesszük). Egy sejt/cella szomszédjai a környezetében lévő sejtek. A játék körökre osztott, a kezdő állapotban tetszőleges számú cellába sejteket helyezünk. Ezt követően a játékosnak nincs beleszólása a játékmenetbe.
        </para>
        <para>
            Egy sejttel (cellával) egy körben a következő három dolog történhet:
            <itemizedlist>
                <listitem>
                    <para>A sejt túléli a kört, ha két vagy három szomszédja van.</para>
                </listitem>
                <listitem>
                    <para>A sejt elpusztul, ha kettőnél kevesebb (elszigetelődés), vagy háromnál több (túlnépesedés) szomszédja van.</para>
                </listitem>
                <listitem>
                    <para>Új sejt születik minden olyan cellában, melynek környezetében pontosan három sejt található.</para>
                </listitem>
            </itemizedlist>
        </para> 
        <para>
            Sejtek egy halmazát alakzatnak hívjuk.
            Néhány ilyen alakzat rövid időn belül kihal (azaz egy sejtje sem marad életben), eltűnik; mások változatlanok maradnak (tengődő alakzatok), és olyanok is léteznek, amelyek ciklikusan önmagukba térnek vissza (pulzáló alakzatok). Az egyik ilyen alakzatot nevezzük siklónak vagy angol nevén glider-nek.
        </para>
        <para>
            A programot lehet vezérelni pár billentyűvel, amik az alábbiak:
            <itemizedlist>
                <listitem><para>k - Megfelezi a cellák méretét</para></listitem>
                <listitem><para>n - Megduplázza a cellák méretét</para></listitem>
                <listitem><para>l - Megduplázza a várakozási időt</para></listitem>
                <listitem><para>g - Megfelezi a várakozási időt</para></listitem>
                <listitem><para>s - Pillanatfelvétel mentése a jelenlegi állapotról (png)</para></listitem>
            </itemizedlist>
            Ezen kívűl az egérrel szintén tudjuk vezérelni a programot, úgy, hogy ahova kattintunk az egérrel ott születik egy új sejt, így rajzolni is tudunk a programba akár egy egész sejtszerkezetet, ha engedi a várakozási időnk.k
        </para>
        <figure>
            <title>A java életjáték egy pillanatképe</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Conway/eletjatek_java.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>A java életjáték egy pillanatképe</phrase>
                </textobject>
            </mediaobject>
        </figure>    
    </section>        
    <section>
        <title>Qt C++ életjáték</title>
        <para>
            Most Qt C++-ban!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/LjHDLc4rTqU">https://youtu.be/LjHDLc4rTqU</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://progpater.blog.hu/2011/03/03/fegyvert_a_nepnek"></link> 
        </para>
        <para>
            GitLab forrás: <link xlink:href="https://gitlab.com/HPeti/bhax/-/tree/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/eletjatek_c">GitLab/HPeti/bhax/Conway/eletjatek_c</link> 
        </para>
        <para>
            Ugyan az a program feladata, annyi különbséggel, hogy itt nincs megvalósítva a billentyűkkel és az egérrel való vezérlés.
        </para>
        <figure>
            <title>A Qt C++ életjáték egy pillanatképe</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Conway/eletjatek_qtcpp.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>A Qt C++ életjáték egy pillanatképe</phrase>
                </textobject>
            </mediaobject>
        </figure>            
    </section>        
    <section>
        <title>BrainB Benchmark</title>
        <para>
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/agUf9wxVp78">https://youtu.be/agUf9wxVp78</link>
        </para>
        <para>
            Program forrása: <link xlink:href="https://gitlab.com/HPeti/bhax/-/tree/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/BrainB">GitLab/HPeti/bhax/Conway/BrainB/</link>
        </para>
        <para>
            Eredmények forrása: <link xlink:href="https://gitlab.com/HPeti/bhax/-/tree/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/BrainB_benchmark">GitLab/HPeti/bhax/Conway/BrainB_benchmark/</link>
        </para>
        <para>
        </para>
        <screen><![CDATA[
hpeti@hpeti-Lenovo-V330-15IKB:~/BrainB$ qmake
hpeti@hpeti-Lenovo-V330-15IKB:~/BrainB$ make
hpeti@hpeti-Lenovo-V330-15IKB:~/BrainB$ ./BrainB
]]>
        </screen>
        <para>
            Itt csak annyi feladatunk van, hogy a benchmark-ot lefuttassuk mi is és teszteljük magunkat.
        </para>
        <para>
            A program feladata az, hogy végig követni kell a bal egérgomb nyomvatartása mellett a <literal>Samu Entropy</literal> objektumot, melynek a közepén található egy kör. Minél hosszabb ideig van a körben az egerünk, annál gyorsabban fogjuk kapni a pontszámokat (<literal>bit/sec</literal>). Emellett, minél több pontunk van, annál több más <literal>Entropy</literal>-k fognak megjelenni a képernyőn - leggyakrabban a <literal>Samu Entropy</literal> közelében - így nehezítve a dolgunkat.
            Ezt mindössze 10 percig kell csinálnunk, eddig tart a benchmark.
        </para>
        <para>
            (Ui.: nem ajánlatos több benchmarkot futtatni egymás után, de megtehetjük ezt is ha akarjuk)
        </para>
        <para>
            A benchmark lefutása után megkapjuk a részletes eredményünket, és egy png képet, a benchmark utolsó képkockájával.
        </para>
        <screen><![CDATA[
NEMESPOR BrainB Test 6.0.3
time      : 6000
bps       : 59940
noc       : 26
nop       : 0
lost      : 
65380 25740 37110 23960 61630 84190 
mean      : 49668
var       : 24383.9
found     : 2100 2060 4510 15530 6910 10650 2080 17700 19440 23760 29630 11090 27940 0 47910 19570 50960 32900 42040 37050 36940 51450 38960 62990 75250 54670 27550 
mean      : 27838
var       : 20525.6
lost2found: 0 19570 50960 37050 54670 
mean      : 32450
var       : 22783.8
found2lost: 25740 37110 23960 61630 84190 
mean      : 46526
var       : 25868.2
mean(lost2found) < mean(found2lost)
time      : 10:0
U R about 4.82031 Kilobytes
]]>
        </screen>
        <figure>
            <title>A BrainB Benchmark utolsó képe (800x600)</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Conway/BrainB_benchmark/Test-6000-screenimage.png" scale="85" />
                </imageobject>
                <textobject>
                    <phrase>A BrainB Benchmark utolsó képe (800x600)</phrase>
                </textobject>
            </mediaobject>
        </figure> 
        <para>
            Esetlegesen még el lehet szórakozni azzal, hogy milyen felbontáson futtatjuk a programon, ugyanis teljes képernyős módba fut a program és a beállított felbontáson megy majd a benchmark. Nagy különbséget viszont nem vettem észre, mivel a felbontás csökkentésével annyit érünk el, hogy nagyobbak lesznek az objektumok láthatóan, de kisebb helyek is tudnak megjelenni.
        </para>
        <para>
            A példa kedvéért én lefuttattam 1920x1080, és másodjára 800x600-as felbontáson (oké, figyelembe véve a 16:9-ről 4:3-as képarányra váltást, de ez itt most mellékes). Az eredmény ugyan ebbe a mappába található meg.
        </para>
        <para>
            Full-HD eredmény: <link xlink:href="https://gitlab.com/HPeti/bhax/-/tree/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/BrainB_benchmark/full_hd_screen">GitLab/HPeti/bhax/Conway/BrainB_benchmark/full_hd_screen</link>
        </para>
    </section>
    <section>
        <title>Vörös Pipacs Pokol/19 RF</title>
        <para>
            Megoldás forrása: <link xlink:href="https://github.com/nbatfai/RedFlowerHell">https://github.com/nbatfai/RedFlowerHell</link>
        </para>
        <para>
            (Eredeti)Megoldás videó: <link xlink:href="https://youtu.be/VP0kfvRYD1Y">https://youtu.be/VP0kfvRYD1Y</link>
        </para>
        <para>
            SmartSteve initial (22 RF): <link xlink:href="https://youtu.be/jN8y-Jlxg78">https://youtu.be/jN8y-Jlxg78</link>
        </para>
    </section>
</chapter>                
